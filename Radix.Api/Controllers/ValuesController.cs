﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Radix.Domain.Models;
using Radix.Domain.Interfaces;
using System.Net.Http;
using System.Net;

namespace Radix.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        public IConfiguration configuration;
        public IAzureQueue _azureQueue;


        public ValuesController(IConfiguration Configuration, IAzureQueue azureQueue)
        {
            configuration = Configuration;
            _azureQueue = azureQueue;
        }

        [HttpPost]
        public async Task<HttpResponseMessage> SendEvent([FromBody] Event item)
        {
            try
            {
                await _azureQueue.SendAsync(item);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch(Exception ex)
            {
                return new
                     HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

        }


    }
}
