﻿using Microsoft.Azure.ServiceBus;
using Radix.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Radix.Domain.Interfaces
{
    public interface IAzureQueue
    {
        Task SendAsync(Event item);
        void ProcessMessage(Message message);
    }
}
