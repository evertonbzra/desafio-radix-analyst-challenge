﻿using Radix.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Radix.Domain.Interfaces
{
    public interface IProcessRepository
    {
        void Save(Sensor sensor);
    }
}
