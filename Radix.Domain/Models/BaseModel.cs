﻿using Radix.Domain.Models.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Radix.Domain.Models
{
    public class BaseModel
    {
        public BaseModel()
        {
            this.Created = DateTimeExtensions.DateTimeBR();
        }

        public long Id { get; set; }
        public DateTime Created { get; set; }
        public virtual bool IsNew()
        {
            return Id == 0;
        }

    }
}
