﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Radix.Domain.Models
{
    public class Event
    {
        public string TimeStamp { get; set; }
        public string Tag { get; set; }
        public string Value { get; set; }
    }
}
