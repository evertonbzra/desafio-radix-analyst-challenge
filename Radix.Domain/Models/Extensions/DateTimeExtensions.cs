﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Radix.Domain.Models.Extensions
{
   public static class DateTimeExtensions
    {
        public static DateTime DateTimeBR()
        {
            var brazilDateTimeNow = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, GetBrazilTimeZone());
            return brazilDateTimeNow;
        }

        private static TimeZoneInfo GetBrazilTimeZone()
        {
            String timeZoneId = "E. South America Standard Time";
            TimeZoneInfo brazilTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            return brazilTimeZone;
        }
    }
}
