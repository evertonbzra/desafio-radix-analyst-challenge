﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Radix.Domain.Models
{
    public class Sensor : BaseModel
    {
        public Sensor() : base() { }

        public string Timestamp { get; set; }
        public string Tag { get; set; }
        public string Value { get; set; }
        public bool Error { get; set; }
    }
}
