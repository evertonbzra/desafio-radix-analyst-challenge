﻿using Radix.Domain.Interfaces;
using Radix.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Radix.Domain.Services
{
    public class AzureService : IAzureQueue
    {
        private QueueClient client;
        private IConfiguration configuration;
        private IProcessRepository processRepository;

        public AzureService(IConfiguration Configuration,IProcessRepository ProcessRepository)
        {
            configuration = Configuration;
            processRepository = ProcessRepository;
            InitClient();
        }

        public async Task SendAsync(Event item)
        {
            var json = JsonConvert.SerializeObject(item);
            var message = new Message(Encoding.UTF8.GetBytes(json));
            await client.SendAsync(message);
        }

        public void ProcessMessage(Message message)
        {
            var messageEncoded = Encoding.UTF8.GetString(message.Body);

            if (!string.IsNullOrEmpty(messageEncoded))
            {
                var sensor = JsonConvert.DeserializeObject<Sensor>(messageEncoded);
                sensor.Error = string.IsNullOrEmpty(sensor.Value) ? true : false;
                processRepository.Save(sensor);
                client.CompleteAsync(message.SystemProperties.LockToken);
            }

        }

        private void InitClient()
        {
            client = new QueueClient(
                configuration.GetSection("ServiceBusConfiguration:ConnectionString").Value,
                configuration.GetSection("ServiceBusConfiguration:QueueName").Value);
        }
    }
}
