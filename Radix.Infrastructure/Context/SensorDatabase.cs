﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Radix.Domain.Models;
using Radix.Infrastructure.Mapping;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Radix.Infrastructure.Context
{
    public class SensorDatabase : DbContext
    {
        public SensorDatabase(DbContextOptions<SensorDatabase> options) : base(options) { }

        public DbSet<Sensor> Sensor { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasDefaultSchema("dbo");
            builder.ApplyConfiguration(new SensorMap());
            base.OnModelCreating(builder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
    }
}
