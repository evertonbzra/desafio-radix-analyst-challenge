﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Radix.Infrastructure.Extensions
{
    public abstract class EntityTypeConfiguration<TEntity> where TEntity: class
    {
        public abstract void Map(EntityTypeBuilder<TEntity> builder);
    }
}
