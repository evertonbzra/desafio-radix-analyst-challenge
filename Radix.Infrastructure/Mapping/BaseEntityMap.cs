﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Radix.Domain.Models;
using Radix.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Radix.Infrastructure.Mapping
{
    public abstract class BaseEntityMap<T> : EntityTypeConfiguration<T> where T : BaseModel
    {
        public override void Map(EntityTypeBuilder<T> builder) { }
    }
}