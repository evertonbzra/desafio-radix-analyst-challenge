﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Radix.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Radix.Infrastructure.Mapping
{
    public class SensorMap : IEntityTypeConfiguration<Sensor>
    {
        public void Configure(EntityTypeBuilder<Sensor> builder)
        {
            builder.ToTable("Sensor");

            builder.HasKey(s => s.Id).HasName("Id");

            builder.Property(s => s.Id)
                .IsRequired()
                .ValueGeneratedOnAdd()
                .HasColumnType("bigint")
                .HasColumnName("Id");


            builder.Property(s => s.Tag)
               .IsRequired()
               .ValueGeneratedOnAdd()
               .HasColumnType("varchar")
               .HasMaxLength(100)
               .HasColumnName("Tag");

            builder.Property(s => s.Timestamp)
               .IsRequired()
               .HasColumnType("varchar")
               .HasMaxLength(100)
               .HasColumnName("Timestamp");

            builder.Property(s => s.Value)
               .IsRequired()
               .HasColumnType("varchar")
               .HasMaxLength(100)
               .HasColumnName("Value");

            builder.Property(s => s.Error)
               .IsRequired()
               .HasColumnType("bit")
               .HasColumnName("Error");

            builder.Property(s => s.Created)
               .IsRequired()
               .HasColumnType("datetime")
               .HasColumnName("InsertDate");

        }

    }
}
