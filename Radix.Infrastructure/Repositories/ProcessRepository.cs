﻿using Radix.Domain.Interfaces;
using Radix.Domain.Models;
using Radix.Infrastructure.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Radix.Infrastructure.Repositories
{
    public class ProcessRepository :  IProcessRepository
    {
        private readonly SensorDatabase DbContext;

        public ProcessRepository(SensorDatabase DatabaseContext)
        {
            DbContext = DatabaseContext;
        }

        public  void Save(Sensor sensor)
        {
            if (sensor == null)
                return;

            DbContext.Sensor.Add(sensor);
            DbContext.SaveChanges();
        }
    }
}
