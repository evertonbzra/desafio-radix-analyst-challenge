﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.ServiceBus;
using Microsoft.Extensions.Configuration;
using Radix.Process.Common.Injection;
using Radix.Process.Common.Process;
using System.IO;
using System.Threading.Tasks;
using Radix.Domain.Interfaces;
using Radix.Infrastructure.Repositories;
using Radix.Domain.Services;
using SimpleInjector;
using Radix.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

namespace Radix.Process.Common
{
    public class Bootstrap
    {
        private static IConfiguration _configuration { get; set; }
        public static void Start()
        {

            IServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();


            var config = new JobHostConfiguration()
            {
                JobActivator = new WebJobActivator(serviceProvider),
                NameResolver = new ConfigurationNameResolver(_configuration)
            };

            config.UseServiceBus(GetConfigurationService(_configuration));
            config.UseTimers();

            var host = new JobHost(config);

            host.RunAndBlock();
        }



        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            var builder = new ConfigurationBuilder()
                            .SetBasePath(ToApplicationPath())
                            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                            .AddEnvironmentVariables();

            _configuration = builder.Build();

            serviceCollection.AddSingleton(_configuration);
            serviceCollection.AddTransient<IAzureQueue, AzureService>();
            serviceCollection.AddTransient<IProcessRepository, ProcessRepository>();
            serviceCollection.AddDbContext<SensorDatabase>(options => options.UseSqlServer(_configuration.GetSection("ConnectionString:SqlServerProvider").Value));
            serviceCollection.AddTransient<WebProcess>();

            Environment.SetEnvironmentVariable("AzureWebJobsDashboard", _configuration.GetSection("ServiceBusConfiguration:AzureWebJobsDashboard").Value);
            Environment.SetEnvironmentVariable("AzureWebJobsStorage", _configuration.GetSection("ServiceBusConfiguration:AzureWebJobsStorage").Value);
        }

        private static ServiceBusConfiguration GetConfigurationService(IConfiguration configuration)
        {
            var connection = configuration.GetSection("ServiceBusConfiguration:ConnectionString").Value;

            ServiceBusConfiguration configurationServiceBus = new ServiceBusConfiguration();
            configurationServiceBus.ConnectionString = connection;
            configurationServiceBus.MessageOptions = new MessageHandlerOptions(ExceptionReceivedHandler) { MaxAutoRenewDuration = new TimeSpan(0, 20, 0) };
            return configurationServiceBus;
        }

        private static string ToApplicationPath()
        {
            var exePath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
            Regex appPathMatcher = new Regex(@"(?<!fil)[A-Za-z]:\\+[\S\s]*?(?=\\+bin)");
            var appRoot = appPathMatcher.Match(exePath).Value;
            return appRoot;
        }

        private static Task ExceptionReceivedHandler(ExceptionReceivedEventArgs exceptionReceivedEventArgs)
        {
            Console.WriteLine($"Message handler encountered an exception {exceptionReceivedEventArgs.Exception}.");
            return Task.CompletedTask;
        }
    }
}
