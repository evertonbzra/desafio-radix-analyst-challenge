﻿using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Radix.Process.Common
{
    public class ConfigurationNameResolver : INameResolver
    {
        private readonly IConfiguration _configuration;
        public ConfigurationNameResolver(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string Resolve(string name)
        {
            switch (name)
            {
                case "sensor-event":
                    return _configuration.GetSection("ServiceBusConfiguration:QueueName").Value;
                default:
                    throw new NotImplementedException($"The name {name} is not configured in ConfigurationNameResolver");
            }
        }

    }
}
