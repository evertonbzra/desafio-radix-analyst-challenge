﻿using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Radix.Domain.Interfaces;
using System;

namespace Radix.Process.Common.Injection
{
    public class WebJobActivator : IJobActivator
    {
        private readonly IServiceProvider _service;
        
        public WebJobActivator(IServiceProvider service)
        {
            _service = service;
        }

        public T CreateInstance<T>()
        {
            var service = _service.GetService<T>();
            return service;
        }
    }
}
