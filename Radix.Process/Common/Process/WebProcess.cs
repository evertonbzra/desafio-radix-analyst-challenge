﻿using System;
using System.Text;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Radix.Domain.Interfaces;
using Radix.Domain.Models;
using Radix.Process.Common.Injection;
using SimpleInjector;

namespace Radix.Process.Common.Process
{
    public class WebProcess
    {
        public IAzureQueue _azureQueue;

        public WebProcess(IAzureQueue azureQueue)
        {
            _azureQueue = azureQueue;
        }


        public void ProcessQueue([ServiceBusTrigger("%sensor-event%")] Message message)
        {
            _azureQueue.ProcessMessage(message);
        }
    }
}
