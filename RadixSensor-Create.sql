
USE [labs-database]

CREATE Table Sensor(
Id bigint identity primary key,
Tag varchar(100),
TimeStamp varchar(100),
Value varchar(100),
Error bit,
InsertDate Datetime 
); 